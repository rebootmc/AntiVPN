package net.geekxboy.antivpn;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class CommandHandler implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

		if (args.length == 0) {
			sender.sendMessage(ChatColor.GRAY + "AntiVPN - Help");
			sender.sendMessage(ChatColor.GRAY + "/antivpn whitelist [name] - Whitelists a player");
			sender.sendMessage(ChatColor.GRAY + "/antivpn reload - Reloads the config");
			return true;
		}

		final String command = args[0].toLowerCase();

		if (command.equals("whitelist")) {
			if (!sender.hasPermission("antivpn.commands.whitelist")) {
				sender.sendMessage(ChatColor.RED + "You don't have permission for that!");
				return true;
			}
			if (args.length == 1) {
				sender.sendMessage(ChatColor.RED + "Usage: /antivpn whitelist [name]");
			} else {
				VPN.getInstance().addPlayerName(args[1]);				
				sender.sendMessage(VPN.getInstance().format(VPN.getInstance().getConfig().getString("Messages.Whitelisted")));
			}
		} else if (command.equals("reload")) {
			if (!sender.hasPermission("antivpn.commands.reload")) {
				sender.sendMessage(ChatColor.RED + "You don't have permission for that!");
				return true;
			}
			VPN.getInstance().reloadConfig();
            sender.sendMessage(VPN.getInstance().format(VPN.getInstance().getConfig().getString("Messages.Reloaded")));			
		}

		return true;
	}
}
