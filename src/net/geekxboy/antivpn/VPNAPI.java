package net.geekxboy.antivpn;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class VPNAPI {

	private static JSONParser jsonParser = new JSONParser();

	public static String getHost(String IP) {
		String key = VPN.getInstance().getConfig().getString("Xioax API Key");
		String keyAdd = "";
		if (!key.equalsIgnoreCase("xxxxxx")) {
			keyAdd = "/" + key;
		}
		
		try {
			String userData = readUrl("http://tools.xioax.com/networking/ip/" + IP + keyAdd );
			Object parsedData = jsonParser.parse(userData);
			if (parsedData instanceof JSONObject) {
				JSONObject jsonData = (JSONObject) parsedData;
				if (jsonData.containsKey("status")) {
					String status = (String) jsonData.get("status");
					if (status.equalsIgnoreCase("success")) {
						return jsonData.containsKey("host-ip") ? String.valueOf(jsonData.get("host-ip").toString()) : null;
					} else {
						return null;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private static String readUrl(String urlString) throws Exception {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);
			return buffer.toString();
		} finally {
			if (reader != null)
				reader.close();
		}
	}

}
