package net.geekxboy.antivpn;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class PlayerListener implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerLoginEvent event) {
		String IP = event.getRealAddress().getHostAddress();
		String IPEscaped = IP.replace(".", "-");
		
		String name = event.getPlayer().getName();
		
		if (VPN.getInstance().isPlayerNameWhitelisted(name)) {
			VPN.getInstance().addPlayerIP(IPEscaped);
			VPN.getInstance().removePlayerName(name);
		} else {
			if (!(VPN.getInstance().isPlayerIPWhitelisted(IPEscaped))) {
				String status = VPNAPI.getHost(IP);
				if (status == null) {
					if (VPN.getInstance().getConfig().getBoolean("Deny on failed lookup")) {
						event.disallow(Result.KICK_OTHER, VPN.getInstance().format(VPN.getInstance().getConfig().getString("Messages.Offline")));
					}
				} else {
					if (status.equalsIgnoreCase("true")) {
						event.disallow(Result.KICK_OTHER, VPN.getInstance().format(VPN.getInstance().getConfig().getString("Messages.Not Allowed")));
					}
				}
			}
		}
	}
	
}
