package net.geekxboy.antivpn;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class VPN extends JavaPlugin {
	// Plugin
	public Logger log = Logger.getLogger("Minecraft");
	public static VPN instance = null;
	public static VPNAPI api = null;

	// Get Config
	public FileConfiguration config;

	public void setupConfig() {
		this.config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
	}

	public void onEnable() {
		this.log = this.getLogger();
		this.log.info(this.getDescription().getFullName() + " is now enabled.");
		instance = this;
		api = new VPNAPI();

		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerListener(), this);

		setupConfig();
		
		getCommand("antivpn").setExecutor(new CommandHandler());

		final File playerFile = new File(getDataFolder(), "whitelist.yml");
		if (!playerFile.exists())
			saveResource("whitelist.yml", true);
		
		getServer().getScheduler().scheduleSyncRepeatingTask(this,
				new Runnable() {
					public void run() {
						FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
						
						for (String type : playerconfig.getConfigurationSection("Whitelisted").getKeys(false)) {
							String string = playerconfig.getString("Whitelisted." + type);
							if (isNumeric(string)) {
								Long sysTime = System.currentTimeMillis();
								Long ipTime = (long) getNumber(string);
								Long add = TimeUnit.DAYS.toMillis(getConfig().getInt("Whitelist For")); 
								if (sysTime > ipTime + add) {
									VPN.getInstance().removePlayerIP(type);
								}
							}
						}
						
						for (String type : playerconfig.getConfigurationSection("Players").getKeys(false)) {
							String string = playerconfig.getString("Players." + type);
							if (isNumeric(string)) {
								Long sysTime = System.currentTimeMillis();
								Long ipTime = (long) getNumber(string);
								Long add = TimeUnit.DAYS.toMillis(getConfig().getInt("Whitelist For")); 
								if (sysTime > ipTime + add) {
									VPN.getInstance().removePlayerName(type);
								}
							}
						}
					}
				}, 1L, 1 * 20L);
	}

	public void onDisable() {
		this.log = this.getLogger();
		this.log.info(this.getDescription().getFullName() + " is now disabled.");
	}
	
	public static VPN getInstance() {
		return instance;
	}

	public static VPNAPI getAPI() {
		return api;
	}
	
	public String colorize(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}

	public String format(String str) {
		return colorize(getConfig().getString("Messages.Prefix") + str);
	}

	public void sendMessage(Player player, String message) {
		player.sendMessage(format(getConfig().getString("Messages." + message)));
	}

	private boolean isNumeric(String val) {
		try {
			int i = Integer.parseInt(val);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	private int getNumber(String val) {
		try {
			int number = Integer.parseInt(val);
			return number;
		} catch (Exception ex) {
			return -1;
		}
	}
	
	public boolean isPlayerNameWhitelisted(String name) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		for (String type : playerconfig.getConfigurationSection("Players").getKeys(false)) {
			if (type.equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isPlayerIPWhitelisted(String ip) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		for (String type : playerconfig.getConfigurationSection("Whitelisted").getKeys(false)) {
			if (type.equalsIgnoreCase(ip)) {
				return true;
			}
		}
		return false;
	}
	
	public Long getPlayerNameTime(String name) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		for (String type : playerconfig.getConfigurationSection("Players").getKeys(false)) {
			if (type.equalsIgnoreCase(name)) {
				String string = playerconfig.getString("Players." + type);
				if (isNumeric(string)) {
					Long ipTime = (long) getNumber(string);
					return ipTime;
				}
			}
		}
		return null;
	}
	
	public Long getPlayerIPTime(String ip) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		for (String type : playerconfig.getConfigurationSection("Whitelisted").getKeys(false)) {
			if (type.equalsIgnoreCase(ip)) {
				String string = playerconfig.getString("Whitelisted." + type);
				if (isNumeric(string)) {
					Long ipTime = (long) getNumber(string);
					return ipTime;
				}
			}
		}
		return null;
	}
	
	public void removePlayerName(String name) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		playerconfig.set("Players." + name, null);
		
		try {
			playerconfig.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removePlayerIP(String ip) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		playerconfig.set("Whitelisted." + ip, null);

		try {
			playerconfig.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPlayerName(String name) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		playerconfig.set("Players." + name, System.currentTimeMillis());
		
		try {
			playerconfig.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPlayerIP(String ip) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		playerconfig.set("Whitelisted." + ip, System.currentTimeMillis());
		
		try {
			playerconfig.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPlayerIP(String ip, Long time) {
		File playerFile = new File(VPN.getInstance().getDataFolder(), "whitelist.yml");
		FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerFile);
		
		playerconfig.set("Whitelisted." + ip, time);
		
		try {
			playerconfig.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
